FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

WORKDIR /app/code

# this is the develop branch with LDAP support
ARG VERSION=04a4d959fd6ba5fa773e47104f41d4d934fe2c62

# get phpservermon
RUN curl -L https://github.com/phpservermon/phpservermon/archive/${VERSION}.tar.gz | tar -zxvf - --strip-components=1

# link to static directories
RUN ln -sf /app/data/config.php /app/code/config.php && \
    rm -rf /app/code/logs && ln -sf /run/phpservermon/logs /app/code/logs

RUN composer update && composer install

# configure apache
RUN rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

# a2dismod perl is required for php mod to be able to use locales
RUN a2disconf other-vhosts-access-log && a2dismod perl
COPY apache/phpservermon.conf /etc/apache2/sites-enabled/phpservermon.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod php8.1 rewrite
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_size 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/phpservermon/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

ADD start.sh encrypt.php config.php /app/pkg/

CMD [ "/app/pkg/start.sh" ]
