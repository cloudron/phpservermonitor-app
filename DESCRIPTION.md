This app packages PHP Server Monitor <upstream>3.6.0-beta</upstream>

## About

PHP Server Monitor is a script that checks whether your websites and servers are up and running. It comes with a web based user interface where you can manage your services and websites, and you can manage users for each server with a mobile number and email address.

## Features:

* Monitor services and websites (see below).
* Email, SMS, Discord, Pushover, Telegram and Jabber notifications.
* View history graphs of uptime and latency.
* User authentication with 2 levels (administrator and regular user).
* Logs of connection errors, outgoing emails and text messages.
* Easy cronjob implementation to automatically check your servers.

