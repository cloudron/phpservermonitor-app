<?php
define('PSM_DB_PREFIX', 'monitor_');
define('PSM_DB_USER', getenv("CLOUDRON_MYSQL_USERNAME"));
define('PSM_DB_PASS', getenv("CLOUDRON_MYSQL_PASSWORD"));
define('PSM_DB_NAME', getenv("CLOUDRON_MYSQL_DATABASE"));
define('PSM_DB_HOST', getenv("CLOUDRON_MYSQL_HOST"));
define('PSM_DB_PORT', getenv("CLOUDRON_MYSQL_PORT"));
define('PSM_BASE_URL', getenv("CLOUDRON_APP_ORIGIN"));
define('PSM_WEBCRON_KEY', '');
define('PSM_PUBLIC', false);
define('CRON_DOWN_INTERVAL', 5);

