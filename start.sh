#!/bin/bash

set -eu

mkdir -p /run/phpservermon/sessions /run/phpservermon/logs

# this removes the password warning
echo -e "[client]\npassword=${CLOUDRON_MYSQL_PASSWORD}" > /run/phpservermon/mysql-extra
readonly mysql="mysql --defaults-file=/run/phpservermon/mysql-extra --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} -P ${CLOUDRON_MYSQL_PORT} ${CLOUDRON_MYSQL_DATABASE}"

update_settings() {
    echo "=> Updating settings"

    # https://github.com/phpservermon/phpservermon/issues/1057
    password_encrypt_key=$($mysql -NB -e "SELECT value from monitor_config WHERE monitor_config.key='password_encrypt_key'")
    smtp_pass_enc=$(php /app/pkg/encrypt.php "${password_encrypt_key}" "${CLOUDRON_MAIL_SMTP_PASSWORD}")

    $mysql -e "UPDATE monitor_config SET value = '0' WHERE monitor_config.key = 'show_update';";

    # set up email - not working because it has to be encrypted beforehand
    $mysql -e "UPDATE monitor_config SET value = '${CLOUDRON_MAIL_SMTP_USERNAME}' WHERE monitor_config.key = 'email_from_email';";
    $mysql -e "UPDATE monitor_config SET value = '1' WHERE monitor_config.key = 'email_smtp';";
    $mysql -e "UPDATE monitor_config SET value = '${CLOUDRON_MAIL_SMTP_SERVER}' WHERE monitor_config.key = 'email_smtp_host';";
    $mysql -e "UPDATE monitor_config SET value = '${CLOUDRON_MAIL_SMTP_PORT}' WHERE monitor_config.key = 'email_smtp_port';";
    $mysql -e "UPDATE monitor_config SET value = '' WHERE monitor_config.key = 'email_smtp_security';";
    $mysql -e "UPDATE monitor_config SET value = '${CLOUDRON_MAIL_SMTP_USERNAME}' WHERE monitor_config.key = 'email_smtp_username';";
    $mysql -e "UPDATE monitor_config SET value = '${smtp_pass_enc}' WHERE monitor_config.key = 'email_smtp_password';";

    # set up ldap
    $mysql -e "REPLACE INTO monitor_config (monitor_config.key, value) VALUES ('dirauth_status', '1');";
    $mysql -e "REPLACE INTO monitor_config (monitor_config.key, value) VALUES ('authdir_type', 'openldap');";
    $mysql -e "REPLACE INTO monitor_config (monitor_config.key, value) VALUES ('authdir_host_locn', '${CLOUDRON_LDAP_HOST}');";
    $mysql -e "REPLACE INTO monitor_config (monitor_config.key, value) VALUES ('authdir_host_port', '${CLOUDRON_LDAP_PORT}');";
    $mysql -e "REPLACE INTO monitor_config (monitor_config.key, value) VALUES ('authdir_basedn', '${CLOUDRON_LDAP_USERS_BASE_DN}');";
    $mysql -e "REPLACE INTO monitor_config (monitor_config.key, value) VALUES ('authdir_usernameattrib', 'username');";
}

setup() {
    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "=> Waiting for apache2 to start"
        sleep 1
    done

    sleep 1

    echo "=> Initializing PHP Server Monitor"

    if ! csrf=$(curl --fail -s -c /tmp/cookiefile 'http://localhost:8000/install.php?action=config' | sed -ne 's/.*name="csrf" value="\(.*\)".*/\1/p'); then
        echo "==> Failed to extract csrf token"
        exit 1
    fi

    sleep 1

    echo "=> Creating admin"
    if ! curl -o /run/phpservermon/setup2 --fail -s -b /tmp/cookiefile 'http://localhost:8000/install.php?action=install' -H 'Content-Type: application/x-www-form-urlencoded' --data-raw "username=admin&password=changeme&password_repeat=changeme&email=admin%40cloudron.local&csrf=${csrf}"; then
        echo "=> Failed to create admin"
        exit 1
    fi

    echo "=> Created admin"
    # install time defaults
    $mysql -e "UPDATE monitor_config SET value = '1' WHERE monitor_config.key = 'email_status';";

    update_settings
}

[[ ! -f /app/data/config.php ]] && cp /app/pkg/config.php /app/data/config.php

if [[ $(mysql --defaults-file=/run/phpservermon/mysql-extra -u${CLOUDRON_MYSQL_USERNAME} -h${CLOUDRON_MYSQL_HOST} -P${CLOUDRON_MYSQL_PORT} -D${CLOUDRON_MYSQL_DATABASE} -e "SHOW TABLES LIKE 'monitor_config'") ]]; then
    echo "=> PHP Server Monitor was already installed"
    update_settings
else
    echo "=> PHP Server Monitor will be setup"
    ( setup ) &
fi

echo "=> Ensure permissions"
chown -R www-data.www-data /app/data /run/phpservermon

echo "=> Run PHP Server Monitor"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"

exec /usr/sbin/apache2 -DFOREGROUND
