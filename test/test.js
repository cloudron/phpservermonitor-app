#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */
/* global xit */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
        done();
    });

    after(function () {
        browser.quit();
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function setSiteTitle() {
        await browser.get(`https://${app.fqdn}/?&mod=config`);
        await waitForElement(By.id('site_title'));
        await browser.findElement(By.id('site_title')).sendKeys(Key.chord(Key.CONTROL, 'a'));
        await browser.findElement(By.id('site_title')).sendKeys('CloudronTestMonitor');
        await browser.findElement(By.xpath('//button[@name="general_submit"]')).click();
        await browser.sleep(2000);
    }

    async function checkSiteTitle() {
        await browser.get(`https://${app.fqdn}`);
        await browser.findElement(By.xpath('//a[text()="CloudronTestMonitor"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//h1[text()="Status"]'));
        await browser.get(`https://${app.fqdn}/?&logout=1`);
        await waitForElement(By.xpath('//h1[text()="Please sign in"]'));
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//h1[text()="Please sign in"]'));

        await browser.findElement(By.id('input-username')).sendKeys(username);
        await browser.findElement(By.id('input-password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();

        await waitForElement(By.xpath('//h1[text()="Status"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login as admin', login.bind(null, 'admin', 'changeme'));
    it('can set site title', setSiteTitle);
    it('can logout as admin', logout);

    it('can login as admin', login.bind(null, username, password));
    it('can logout as admin', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can check site title', checkSiteTitle);

    it('can login as admin', login.bind(null, username, password));
    it('can logout as admin', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login as admin', login.bind(null, username, password));
    it('can check site title', checkSiteTitle);
    it('can logout as admin', logout);

    it('can login as admin', login.bind(null, username, password));
    it('can logout as admin', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login as admin', login.bind(null, 'admin', 'changeme'));
    it('can check site title', checkSiteTitle);
    it('can logout as admin', logout);

    it('can login as admin', login.bind(null, username, password));
    it('can logout as admin', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login as admin', login.bind(null, 'admin', 'changeme'));
    it('can set site title', setSiteTitle);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can check site title', checkSiteTitle);

    it('can login as admin', login.bind(null, username, password));
    it('can logout as admin', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
